Python 3.8.2 (tags/v3.8.2:7b3ab59, Feb 25 2020, 23:03:10) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license()" for more information.
>>> 
== RESTART: C:\Users\Cordoba\AppData\Local\Programs\Python\Python38\SLANG2.py ==

print("\n\t=================================="
      "\n\t PROGRAMA DE PYTHON usando MariaDB"
      "\n\t CARLOS CORDOBA PIZARRO - II AÑO"
      "\n\t UNIVERSIDAD LATINA DE PANAMA 2020"
      "\n\t==================================\n\t")

# Modulos a Importar
opc = 0
# Importar modulo conector MariaDB/Python
import mariadb
import sys
#MySQL = "CREATE DATABASE 'C:\Program Files\MariaDB 10.4\data\bdslang'"
# Instanciar la conexion
# Conectarse a la plataforma mariadb usando la funcion connect()
try:
    miconexion = mariadb.connect(host="localhost", user="root", password="", database="bdslang")
except mariadb.Error as err:
    print(f"Error en la conexion con la plataforma mariadb:{err}")
    sys.exit(1)
micursor = miconexion.cursor()

def INSERTAR():
    miPalabra = str(input("\n\tDigite la palabra a insertar: "))
    miSignificado = str(input("\tDigite el significado: "))
    try:
        micursor.execute("INSERT INTO tbpalabras(Palabra, Significado) VALUES(?,?)",(miPalabra,miSignificado))
    except mariadb.Error as e:
        print(f"Error:{e}")
    miconexion.commit()

def MODIFICAR():
    miPalabra = str(input("\n\tDigite la palabra a modificar: "))
    micursor.execute("SELECT Palabra,Significado FROM tbpalabras WHERE Palabra =?", (miPalabra,))
    for Palabra in micursor:
        print(f"\tPALABRA A MODIFICAR: {Palabra}")
    modiPalabra = str(input("\tDigite la palabra modificada: "))
    micursor.execute("UPDATE tbpalabras SET Palabra ='%s'WHERE Palabra = '%s'"%(modiPalabra,miPalabra))
    modiSignificado = str(input("\tDigite el significado: "))
    micursor.execute("UPDATE tbpalabras SET Significado = '%s' WHERE Palabra = '%s'" % (modiSignificado,modiPalabra))
    miconexion.commit()
    micursor.execute("SELECT Palabra,Significado FROM tbpalabras WHERE Palabra =?", (modiPalabra,))
    for Palabra in micursor:
        print(f"\tLa palabra se modifico exitosamente: {Palabra}")

def ELIMINAR():
    elemPalabra = str(input("\tIntroduce la palabra que desea eliminar: "))
    micursor.execute("DELETE FROM tbpalabras WHERE Palabra ='%s'" % (str(elemPalabra)))
    miconexion.commit()

def BUSCAR():
    miPalabra = str(input("\n\tDigite una palabra: "))
    micursor.execute("SELECT Palabra,Significado FROM tbpalabras WHERE Palabra =?",(miPalabra,))
    for Palabra in micursor:
        print(f"\tEl significado de la Palabra: {Palabra}")

def IMPRIMIR():
    micursor.execute("SELECT * FROM tbpalabras")
    print("\n\tTABLA DE PALABRAS Y SIGNIFICADOS")
    for Palabra in micursor:
        print(f"\tPalabra:{Palabra}")

while opc != 6:
    print("\n\tMENU DE OPCIONES"
          "\n\t1-Insertar"
          "\n\t2-Modificar"
          "\n\t3-Eliminar"
          "\n\t4-Buscar"
          "\n\t5-Imprimir"
          "\n\t6-Salir de la aplicacion")
    opc = int(input("\n\tDigite una opcion: "))
    if opc == 1:
        INSERTAR()
    if opc == 2:
        MODIFICAR()
    if opc == 3:
        ELIMINAR()
    if opc == 4:
        BUSCAR()
    if opc == 5:
        IMPRIMIR()

print("\n\tFIN DEL PROGRAMA")
